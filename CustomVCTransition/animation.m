//
//  animation.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "animation.h"
#import "ViewController.h"

@implementation animation


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 1;
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    if(self.reverse){
        
        ViewController * toViewController = (ViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
        UIViewController * fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        
        [[transitionContext containerView] addSubview:toViewController.view];
        toViewController.image.transform = CGAffineTransformMakeScale(2.8f, 2.8f);
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
           toViewController.image.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
        } completion:^(BOOL finished) {
            
            fromViewController.view.alpha = 0;
            toViewController.view.transform = CGAffineTransformIdentity;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            
        }];
    
    }else{
        
        UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
        ViewController * fromViewController = (ViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        [[transitionContext containerView] addSubview:toViewController.view];
        toViewController.view.alpha = 0;
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            fromViewController.image.transform = CGAffineTransformMakeScale(2.8f, 2.8f);
            
        } completion:^(BOOL finished) {
            toViewController.view.alpha = 1;
             fromViewController.image.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            fromViewController.view.transform = CGAffineTransformIdentity;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            
        }];
        
    }
    
    
    
    
}

@end
