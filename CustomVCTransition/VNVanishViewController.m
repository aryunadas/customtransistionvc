//
//  VNVanishViewController.m
//  Vanish
//
//  Created by Aryuna on 7/29/14.
//  Copyright (c) 2014 Reyneiro Hernandez. All rights reserved.
//

#import "VNVanishViewController.h"
#import <UIImage+ImageEffects.h>

@interface VNVanishViewController ()
@property (nonatomic, strong) UIView * wrapperView;
@property (nonatomic, strong) UIView * explodeView;
@property (nonatomic, strong) UIView * container;

@property (nonatomic, strong) UIImageView * centerImageView;


@property (nonatomic, strong) UIImageView *envelopFront;
@property (nonatomic, strong) UIImageView *fold;
@property (nonatomic, strong) UIImageView *envelop1;
@end

@implementation VNVanishViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage *  image = self.image;
    
    CATransform3D transform = CATransform3DIdentity;
    transform.m34 = 1.0 / 500.0;
    self.view.layer.transform = transform;
    
    self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.parentFrame.size.width, self.parentFrame.size.height)];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[image applyBlurWithRadius:3.0
                                                                                 tintColor:[UIColor colorWithRed:17/255.0 green:47/255.0 blue:87/255.0 alpha:0.7]
                                                                     saturationDeltaFactor:1.8
                                                                                 maskImage:nil]];
    imageView.frame = self.container.bounds;
    [self.view addSubview:imageView];
    
    
    self.wrapperView = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.wrapperView];
    
    CATransform3D t = CATransform3DIdentity;
    t = CATransform3DTranslate(t, 0, 0, 200);
    self.wrapperView.layer.transform = t;
    
    [self foldView:self.container image:image withCompletions:^{
        [self sobreForContainerView:self.container WithCompletions:^{
            self.complition();
        }];
    }];
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)sobreForContainerView:(UIView *)container WithCompletions:(void (^)(void))block{
    
    float height = container.bounds.size.height/3;
    
    UIImage * envelopeBackground = [UIImage imageNamed:@"Sobre-A"];
    UIImage * foldBackground = [UIImage imageNamed:@"Fold"];
    
    self.envelop1= [[UIImageView alloc]
                            initWithFrame:CGRectMake(67, container.bounds.size.height + foldBackground.size.height , envelopeBackground.size.width ,
                            envelopeBackground.size.height )];
    
     self.envelop1.image = envelopeBackground;
    [self.wrapperView insertSubview:self.envelop1 atIndex:[[self.wrapperView subviews] indexOfObject:container]];
    
    
   
    self.fold= [[UIImageView alloc] initWithFrame:CGRectMake(67, self.envelop1.y  - 43 , foldBackground.size.width , foldBackground.size.height )];
    self.fold.image = foldBackground;
    self.fold.layer.anchorPoint = CGPointMake(0.5, 1);
    [self.wrapperView insertSubview:self.fold atIndex:[[self.wrapperView subviews] indexOfObject:container]];
    
    
    self.envelopFront= [[UIImageView alloc] initWithFrame:self.envelop1.frame];
    self.envelopFront.image = envelopeBackground;
    [self.wrapperView addSubview:self.envelopFront];
    
    int posy = ([[UIScreen mainScreen] bounds].size.height == 480)? 211 : 236;
    
    [UIView animateWithDuration:0.8 animations:^{
        
        CGRect frame  = self.envelopFront.frame;
        frame.origin.y = (container.bounds.size.height/2 - posy) + height;
        self.envelopFront.frame = frame;
        
        frame.origin.y -= 10;
        self.envelop1.frame = frame;
        
        self.fold.y = frame.origin.y - 85;
        
    } completion:^(BOOL finished) {
        
        [self.envelopFront.superview bringSubviewToFront:self.fold];
        
        [CATransaction begin];
        
         CABasicAnimation* animation3 =[self animationWithKey:@"transform.rotation.x" from:@(0) to:@(-180 * M_PI/180) Duration:0.8];
        
        [CATransaction setCompletionBlock:^{
           [self envelopeGoAwayWithCompletions:block];
        }];
        [self.fold.layer addAnimation:animation3 forKey:@"rotation"];
        [CATransaction commit];
       
    }];
    
    
}


-(void)envelopeGoAwayWithCompletions:(void (^)(void))block{
    
    [CATransaction begin];
    self.container.alpha = 0;
    
    CABasicAnimation* animation4 =[self animationWithKey:@"transform.rotation.y" from:@(0) to:@(180 * M_PI/180) Duration:0.6];
    
    [CATransaction setCompletionBlock:^{
          [self explode];
        
        [UIView animateWithDuration:0.1 animations:^{
            self.wrapperView.alpha = 0;
            
        } completion:^(BOOL finished) {
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                block();
            });
            
        }];
       
        
    }];
    [self.wrapperView.layer addAnimation:animation4 forKey:@"rotation"];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        self.envelopFront.alpha = 0;
        self.envelop1.image = nil;
        self.envelop1.backgroundColor = VNColorBlueSky;
        self.fold.alpha = 0;
    });
    [CATransaction commit];
    
}

-(void)explode{
    
    self.explodeView = [[UIView alloc] initWithFrame:self.view.frame];
    [self.view insertSubview:self.explodeView atIndex:6];
    
    int posy= self.envelop1.y  ;
    int posx =  self.envelop1.x;
    int rowsX = 17;
    int rowsY = 9;
    
    float blockWidth = (self.envelop1.width + 5)/rowsX  ;
    float blockHeight = (self.envelop1.height/rowsY) + 0.3;
    int i = 0;
    
    for (i = 0; i < rowsX * rowsY; i++) {
        
        int m = i % rowsY;
        if(m == 0){
            if(i > 0){
             posx += blockWidth ;
            }
            posy = self.envelop1.y ;
        }else{
            posy += blockHeight ;
        }
        
        UIView * c = [[UIView alloc] initWithFrame:CGRectMake( posx, posy, blockWidth, blockHeight)];
        c.tag= i;
        c.backgroundColor = VNColorBlueSky;
        [self.explodeView addSubview:c];
        
    }
    
    int max = 700;
    int min = 400;
    int positiveCursor = 0;
    
    [UIView animateWithDuration:4  delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.explodeView.alpha = 0.7;
    } completion:nil];
    
    
         for (i = [self.explodeView.subviews count] ; i >= 0 ; i--) {
             
             UIView * view =  self.explodeView.subviews[i];
             
             int randNum = rand() % (max - min) + min;
             int randdur = (rand() % (4 - 1)) + 1;
             float delay = randdur * positiveCursor;
             float duration = (rand() % (4 - 2)) + 2;
             
             [UIView animateWithDuration:0.2  delay:(delay/100) -0.25 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                 if((i % 2) == 0 ||  (i % 3) == 0 ){
                     view.alpha = 0.3;
                 }
             } completion:nil];
             
             
             [UIView animateWithDuration:duration/10  delay:(delay/100)  options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    CGRect f =  view.frame;
                     f.origin.y = 110;
                     f.origin.x = randNum;
                     view.frame = f;
                 
             } completion:nil];
             
              positiveCursor ++;
             
         }
    
    
}


- (void)foldView:(UIView *)container image:(UIImage *)image withCompletions:(void (^)(void))block{
    
    float heightitem = container.bounds.size.height / 3;
    float contentp = 0.333333;
    
    UIImageView * topImageView = [[UIImageView alloc] init];
    topImageView.frame = CGRectMake(0, heightitem / 2 , container.bounds.size.width, heightitem);
    topImageView.layer.anchorPoint = CGPointMake(0.5,1);
    topImageView.backgroundColor = VNColorBlueSky8;
    topImageView.layer.allowsEdgeAntialiasing = YES;
    
    CALayer * topback = [[CALayer alloc] init];
    topback.frame = topImageView.frame;
    topback.contentsRect = CGRectMake(0, 0, 1, contentp);
    topback.contents = (id)(image.CGImage);
    topback.allowsEdgeAntialiasing = YES;
    
    
    [topImageView.layer insertSublayer:topback atIndex:0];
    [container insertSubview:topImageView atIndex:5];
    
    self.centerImageView = [[UIImageView alloc] initWithImage:image];
    self.centerImageView.frame = CGRectMake(0,  heightitem -0.2  , container.bounds.size.width, heightitem - 0.2);
    self.centerImageView.layer.contentsRect = CGRectMake(0, contentp, 1, contentp);
    self.centerImageView.layer.allowsEdgeAntialiasing = YES;
    [container insertSubview:self.centerImageView atIndex:0 ];
    
    UIImageView * bottomImageView = [[UIImageView alloc] init];
    bottomImageView.frame = CGRectMake(0,  (heightitem / 2) * 3 , container.bounds.size.width, heightitem);
    bottomImageView.layer.anchorPoint = CGPointMake(0.5,0);
    bottomImageView.backgroundColor = VNColorBlue1;
    bottomImageView.layer.allowsEdgeAntialiasing = YES;
    
    CALayer * bottomback = [[CALayer alloc] init];
    bottomback.frame = CGRectMake(0,  0 , container.bounds.size.width, heightitem);
    bottomback.contentsRect = CGRectMake(0, contentp *2, 1, contentp);
    bottomback.contents = ( id)(image.CGImage);
    bottomback.allowsEdgeAntialiasing = YES;
    
    [bottomImageView.layer insertSublayer:bottomback atIndex:0];
    [container insertSubview:bottomImageView atIndex:2];
    
    
    
    [self.wrapperView addSubview:container];
    container.layer.anchorPoint = CGPointMake(0.5,0.5);
    container.layer.allowsEdgeAntialiasing = YES;
    
    CATransform3D perspective = CATransform3DIdentity;
    perspective.m34 = -1.0/1000;
    container.layer.sublayerTransform = perspective;
    
    
    
    [CATransaction begin];
    
    [self animationWithKey:@"transform.scale" from:@(1) to:@(0.568)
          layer:container.layer generalKey:@"transform" Duration:1];
    
   [CATransaction setCompletionBlock:^{
        
         [CATransaction begin];
            
         [self animationWithKey:@"transform.rotation.x" from:@(0) to:@(-177 * M_PI/180)
                  layer:topImageView.layer generalKey:@"rotation" Duration:0.6];
            
         [CATransaction setCompletionBlock:^{
                block();
         }];
       
         [self animationWithKey:@"opacity" from:@(1) to:@(0)
                        layer:topback generalKey:@"opacity" Duration:0.6];
         [CATransaction commit];
        
    }];
    
    [self animationWithKey:@"transform.rotation.x" from:@(0) to:@(180 * M_PI/180)
                     layer:bottomImageView.layer generalKey:@"rotation" Duration:0.6];
    
    [self animationWithKey:@"opacity" from:@(1) to:@(0)
                     layer:bottomback generalKey:@"opacity" Duration:1];
    
    [CATransaction commit];
}


-(void)animationWithKey:(NSString*) key from:(NSNumber*) from to:(NSNumber*)to layer:(CALayer*) layer generalKey:(NSString*) generalKey Duration: (float) duration{
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:key];
    animation.fromValue = from;
    animation.toValue = to;
    animation.duration = duration;
    [animation setFillMode:kCAFillModeForwards];
    [animation setRemovedOnCompletion:NO];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    [layer addAnimation:animation forKey:generalKey];
    
}


-(CABasicAnimation*)animationWithKey:(NSString*) key from:(NSNumber*) from to:(NSNumber*)to Duration: (float) duration{
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:key];
    animation.fromValue = from;
    animation.toValue = to;
    animation.duration = duration;
    [animation setFillMode:kCAFillModeForwards];
    [animation setRemovedOnCompletion:NO];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    
    return animation;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
