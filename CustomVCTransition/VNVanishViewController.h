//
//  VNVanishViewController.h
//  Vanish
//
//  Created by Aryuna on 7/29/14.
//  Copyright (c) 2014 Reyneiro Hernandez. All rights reserved.
//

#import "VNBaseViewController.h"

typedef void(^complitionBlock)();

@interface VNVanishViewController : UIViewController
@property(nonatomic, retain) UIImage * image;
@property(nonatomic) CGRect parentFrame;
@property (strong, nonatomic) complitionBlock complition;
@end
