//
//  animation.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "animations3.h"
#import "ViewControllers3.h"

@implementation animations3


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 2;
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    if(self.reverse){
        
        ViewControllers3 * toViewController = (ViewControllers3*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
        UIViewController * fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        
        [[transitionContext containerView] addSubview:toViewController.view];
       
        
        CATransform3D perspectiveTransform = CATransform3DIdentity;
        perspectiveTransform.m34 = -0.001;
        toViewController.view.layer.sublayerTransform = perspectiveTransform;
        
        [CATransaction begin];
        
        CABasicAnimation* animation =  [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
        animation.fromValue = @(0);
        animation.toValue = @(360 * M_PI/180);
        animation.duration = 2;
        [animation setRemovedOnCompletion:NO];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        
        CABasicAnimation* animation2 =  [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        animation2.fromValue = @(2.8);
        animation2.toValue = @(1);
        animation2.duration = 2;
        [animation2 setRemovedOnCompletion:NO];
        animation2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        [CATransaction setCompletionBlock:^{
            fromViewController.view.alpha = 0;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            
        }];
        [toViewController.image.layer addAnimation:animation forKey:@"transform"];
        [toViewController.image.layer addAnimation:animation2 forKey:@"transformm"];
        [CATransaction commit];
    
    }else{
        
        UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
        ViewControllers3 * fromViewController = (ViewControllers3*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        [[transitionContext containerView] addSubview:toViewController.view];
        toViewController.view.alpha = 0;
        
        CATransform3D perspectiveTransform = CATransform3DIdentity;
        perspectiveTransform.m34 = -0.001;
        fromViewController.view.layer.sublayerTransform = perspectiveTransform;
        
        [CATransaction begin];
        
        CABasicAnimation* animation =  [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
        animation.fromValue = @(0);
        animation.toValue = @(-360 * M_PI/180);
        animation.duration = 2;
        [animation setRemovedOnCompletion:NO];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        
        
        CABasicAnimation* animation2 =  [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        animation2.fromValue = @(1);
        animation2.toValue = @(2.8);
        animation2.duration = 2;
        [animation2 setRemovedOnCompletion:NO];
        animation2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
       
        
        
        [CATransaction setCompletionBlock:^{
             toViewController.view.alpha = 1;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        
        }];
         [fromViewController.image.layer addAnimation:animation forKey:@"transform"];
         [fromViewController.image.layer addAnimation:animation2 forKey:@"transformm"];
        [CATransaction commit];
        
    }
    
    
    
    
}


-(void)animation{
    
   
}

@end
