//
//  ViewController.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "ViewControllers3.h"
#import "DetailViewControllers3.h"
#import "animations3.h"

@interface ViewControllers3 ()
- (IBAction)toDetailViewcontroller:(id)sender;


@end

@implementation ViewControllers3

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    
    
    animations3 *animationController = [[animations3 alloc] init];
    animationController.reverse = (operation == UINavigationControllerOperationPush)? NO : YES;
    return animationController;
}




- (IBAction)toDetailViewcontroller:(id)sender {
    
   UIStoryboard * storyboard =  [UIStoryboard storyboardWithName:@"step3" bundle:nil ];
    DetailViewControllers3 * incoming = [storyboard instantiateViewControllerWithIdentifier:@"detailVC"];
    //[self presentViewController:incoming animated:YES completion:nil];
   
    //[self performSegueWithIdentifier:@"toDetail" sender:self];
    self.navigationController.delegate = self;
    [self.navigationController pushViewController:incoming animated:YES];
    
}


@end
