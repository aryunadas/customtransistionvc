//
//  VNBaseViewController.h
//  Vanish
//
//  Created by Reyneiro Hernandez on 5/28/14.
//  Copyright (c) 2014 Reyneiro Hernandez. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VNBaseViewController : UIViewController



-(void)deallocating;

-(void)customizeAppearance;
-(void)databinding;

//Create Menu
-(void)toggleCreateMenu;
-(void)hideCreateMenu;
-(void)addCreateRecordCategoryView;

// Activity Indicator
+(void)showActivityIndicatorInTableView:(UITableView *)tableView tintColor:(UIColor *)tintColor;
+(void)hideActivityIndicatorInTableView:(UITableView *)tableView;

@end
