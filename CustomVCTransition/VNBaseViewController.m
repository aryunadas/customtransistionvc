//
//  VNBaseViewController.m
//  Vanish
//
//  Created by Reyneiro Hernandez on 5/28/14.
//  Copyright (c) 2014 Reyneiro Hernandez. All rights reserved.
//

#import "VNBaseViewController.h"


@interface VNBaseViewController ()

@property (nonatomic) BOOL isCreateRecordCategoryViewVisible;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *createButton;

@end

@implementation VNBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)customizeAppearance{
}

-(void)databinding{
    

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)deallocating{
    
}

-(void)dealloc{
    [self deallocating];
    NSLog(@"DEALLOCATING: ****%@*****", NSStringFromClass([self class]));
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)addCreateRecordCategoryView{
  
    
   
}





#pragma mark - Activity Inidicator

+(void)showActivityIndicatorInTableView:(UITableView *)tableView tintColor:(UIColor *)tintColor
{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    [activityIndicator setColor:tintColor];
    [activityIndicator setBackgroundColor:[UIColor clearColor]];
    [activityIndicator startAnimating];
    activityIndicator.clipsToBounds = YES;
    tableView.tableFooterView = activityIndicator;
}

+(void)hideActivityIndicatorInTableView:(UITableView *)tableView
{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:.3 animations:^{
                [tableView.tableFooterView setTransform:CGAffineTransformMakeScale(0, 0)];

            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.2 animations:^{
                    tableView.tableFooterView = [[UIView alloc] init];
                }];
            }];
        });
}

@end
