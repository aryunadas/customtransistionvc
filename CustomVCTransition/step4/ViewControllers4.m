//
//  ViewController.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "ViewControllers4.h"
#import "DetailViewControllers4.h"
#import "animations4.h"

@interface ViewControllers4 ()
- (IBAction)toDetailViewcontroller:(id)sender;


@end

@implementation ViewControllers4

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    
    
    animations4 *animationController = [[animations4 alloc] init];
    animationController.reverse = (operation == UINavigationControllerOperationPush)? NO : YES;
    return animationController;
}




/*- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                         interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    
    animation *animationController = [[animation alloc] init];
    animationController.reverse = YES;
    return animationController;
    
}*/


/*- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    animation *animationController = [[animation alloc] init];
    return animationController;
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
   animation *animationController = [[animation alloc] init];
    animationController.reverse = YES;
    return animationController;
}*/





- (IBAction)toDetailViewcontroller:(id)sender {
    
   UIStoryboard * storyboard =  [UIStoryboard storyboardWithName:@"step3" bundle:nil ];
    DetailViewControllers4 * incoming = [storyboard instantiateViewControllerWithIdentifier:@"detailVC"];
    //[self presentViewController:incoming animated:YES completion:nil];
   
    //[self performSegueWithIdentifier:@"toDetail" sender:self];
    self.navigationController.delegate = self;
    [self.navigationController pushViewController:incoming animated:YES];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UIViewController *  toViewcontroller = (UIViewController*)segue.destinationViewController;
    //toViewcontroller.transitioningDelegate = self;
    toViewcontroller.modalPresentationStyle = UIModalPresentationCustom;
}
@end
