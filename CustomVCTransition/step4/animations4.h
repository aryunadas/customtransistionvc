//
//  animation.h
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface animations4 : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL reverse;
@end
