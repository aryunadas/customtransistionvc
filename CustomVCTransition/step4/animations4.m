//
//  animation.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "animations4.h"
#import "ViewControllers4.h"

@implementation animations4


- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return 2;
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    if(self.reverse){
        
        ViewControllers4 * toViewController = (ViewControllers4*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
        UIViewController * fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        
        [[transitionContext containerView] addSubview:toViewController.view];
       
        
        CATransform3D perspectiveTransform = CATransform3DIdentity;
        perspectiveTransform.m34 = -0.001;
        toViewController.view.layer.sublayerTransform = perspectiveTransform;
        
        toViewController.image.layer.easeInEaseOut.duration = 2;
        [NSObject animate:^{
            
            toViewController.image.layer.easeInEaseOut.pop_rotationX = 360 * M_PI/180;
            toViewController.image.layer.easeInEaseOut.pop_scaleXY = CGPointMake(1, 1);
        } completion:^(BOOL finish){
            fromViewController.view.alpha = 0;
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
    
    }else{
        
        UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        
        ViewControllers4 * fromViewController = (ViewControllers4*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        
        [[transitionContext containerView] addSubview:toViewController.view];
        toViewController.view.alpha = 0;
        
        CATransform3D perspectiveTransform = CATransform3DIdentity;
        perspectiveTransform.m34 = -0.001;
        fromViewController.view.layer.sublayerTransform = perspectiveTransform;
        
        fromViewController.image.layer.easeInEaseOut.duration = 2;
        [NSObject animate:^{
            
            fromViewController.image.layer.easeInEaseOut.pop_rotationX = -360 * M_PI/180;
            fromViewController.image.layer.easeInEaseOut.pop_scaleXY = CGPointMake(2.8, 2.8);
        } completion:^(BOOL finish){
            toViewController.view.alpha = 1;
           [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        }];
        
        
    }
    
    
    
    
}


-(void)animation{
    
   
}

@end
