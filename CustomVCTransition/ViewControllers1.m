//
//  ViewController.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "ViewControllers1.h"
#import "DetailViewControllers1.h"
#import "animations1.h"

@interface ViewControllers1 ()
- (IBAction)toDetailViewcontroller:(id)sender;

@end

@implementation ViewControllers1

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    
}*/

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    animations1 *animationController = [[animations1 alloc] init];
    return animationController;
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
   animations1 *animationController = [[animations1 alloc] init];
   return animationController;
}



- (IBAction)toDetailViewcontroller:(id)sender {
    
   UIStoryboard * storyboard =  [UIStoryboard storyboardWithName:@"step1" bundle:nil ];
    DetailViewControllers1 * incoming = [storyboard instantiateViewControllerWithIdentifier:@"detailVC"];
    incoming.transitioningDelegate = self;
   [self presentViewController:incoming animated:YES completion:nil]; 
    
}
@end
