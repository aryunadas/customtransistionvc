//
//  ViewController.m
//  animationtest
//
//  Created by jaganath on 6/14/14.
//  Copyright (c) 2014 jaganath. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIImage *  im = [UIImage imageNamed:@"image.jpg"];
    
    [self foldimage:im];
  
    
}

- (void) foldimage: (UIImage*) im{
    
    UIView * container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width , self.view.bounds.size.height)];
    
    int heightitem = self.view.bounds.size.height / 3;
    
    UIImageView * im1 = [[UIImageView alloc] initWithImage:im];
    im1.frame = CGRectMake(0, heightitem / 2 + 1, self.view.bounds.size.width, heightitem);
    im1.layer.contentsRect = CGRectMake(0, 0, 1, 0.33);
    im1.layer.anchorPoint = CGPointMake(0.5,1);
    [container insertSubview:im1 atIndex:2];
    
    
    UIImageView * im2 = [[UIImageView alloc] initWithImage:im];
    im2.frame = CGRectMake(0,  heightitem , self.view.bounds.size.width, heightitem);
    im2.layer.contentsRect = CGRectMake(0, 0.33, 1, 0.33);
    [container insertSubview:im2 atIndex:0 ];
    
    UIImageView * im3 = [[UIImageView alloc] initWithImage:im];
    im3.frame = CGRectMake(0,  (heightitem / 2) * 3 , self.view.bounds.size.width, heightitem);
    im3.layer.contentsRect = CGRectMake(0, 0.66, 1, 0.33);
    im3.layer.anchorPoint = CGPointMake(0.5,0);
    [container insertSubview:im3 atIndex:4];
    
    
    
    [self.view addSubview:container];
    container.layer.anchorPoint = CGPointMake(0.5,0.5);
    
    CATransform3D perspective = CATransform3DIdentity;
    perspective.m34 = -1.0/1000;
    container.layer.sublayerTransform = perspective;
    
    [UIView animateWithDuration:0.7f
                     animations:^
     {
         
         container.transform = CGAffineTransformScale(container.transform, 0.7, 0.7);
         
     }
       completion:^(BOOL finished)
     {
         
     }
     ];
    
    /*[CATransaction begin];
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.fromValue = @(1);
    animation.toValue = @(0.8);
    animation.duration = 2.0;
    [animation setFillMode:kCAFillModeForwards];
    [animation setRemovedOnCompletion:NO];*/
    
    
    
    
    //---------------------------------------------------------
    
    
    [CATransaction begin];
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
    animation.fromValue = @(0);
    animation.toValue = @(-180 * M_PI/180);
    animation.duration = 1.1;
    [animation setFillMode:kCAFillModeForwards];
    [animation setRemovedOnCompletion:NO];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    
    [CATransaction setCompletionBlock:^{
        
        
        [CATransaction begin];
        CABasicAnimation* animation3 = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
        animation3.fromValue = @(0);
        animation3.toValue = @(180 * M_PI/180);
        animation3.duration = 1.1;
        [animation3 setFillMode:kCAFillModeForwards];
        [animation3 setRemovedOnCompletion:NO];
        [animation3 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [CATransaction setCompletionBlock:^{
            
        }];
        [im3.layer addAnimation:animation3 forKey:@"rotation"];
        [CATransaction commit];
        
    }];
    [im1.layer addAnimation:animation forKey:@"rotation"];
    [CATransaction commit];
    
    
    
   
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
