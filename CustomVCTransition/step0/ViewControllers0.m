//
//  ViewController.m
//  CustomVCTransition
//
//  Created by jaganath on 9/14/14.
//  Copyright (c) 2014 rokk3rlabs. All rights reserved.
//

#import "ViewControllers0.h"
#import "DetailViewControllers0.h"


@interface ViewControllers0 ()
- (IBAction)toDetailViewcontroller:(id)sender;

@end

@implementation ViewControllers0

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)toDetailViewcontroller:(id)sender {
    
   UIStoryboard * storyboard =  [UIStoryboard storyboardWithName:@"step1" bundle:nil ];
    DetailViewControllers0 * incoming = [storyboard instantiateViewControllerWithIdentifier:@"detailVC"];
    incoming.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
   [self presentViewController:incoming animated:YES completion:nil]; 
    
}
@end
